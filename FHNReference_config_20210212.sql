-- Script Name:       FHNReference_config_20210212.sql
-- Generated:         2021-02-12 13:57:25 CST
-- Model Name:        FHNReference
-- Model FileName:    /FHNReferenceModel/FHNReferenceModel.emx
-- Model Version:     2021-02-05 07:01:46
-- Extractor Version: 3.2.4.0_v20200619
-- Database Type:     DB2
-- Database Version:  FTM v3.2.4.0-v3.2.4.x
-----------------------------------
-- Referenced Models:
-----------------------------------
--
-- Model Name:        FTM Generic Model
-- Model fileName:    /FTM Generic Model/FTM Generic Model.emx
-- Model Version:     2021-02-03 12:25:00
--
-- Model Name:        FTM Feature Model
-- Model fileName:    /FTM Feature Model/FTM Feature Model.emx
-- Model Version:     2021-02-03 15:55:18
--
-- Model Name:        CPS Reference
-- Model fileName:    /CPSReferenceModel/CPS Reference Model.emx
-- Model Version:     2021-02-02 15:50:28

SET CURRENT SCHEMA = 'FTM';

SET CURRENT PATH = 'FTM'; 


CALL CHECK_SYNCH(
'FHNReference',
'3.2.4'
);



CALL SET_PENDING(
'FHNReference',
'3.2.4'
);


CALL UPSERT_APPLICATION_V3240(
'FHNReference',
NULL
);

-- *************************************************
-- *
-- *  UPSERT_VERSION
-- *
-- *  Parameters:
-- *          1. APP_NAME - Application name
-- *          2. APP_VERSION - Application version
-- *          3. EFFECTIVE_DATETIME
-- *          4. AUTO_REFRESH_AFTER
-- *          5. MODEL_NAME
-- *          6. MODEL_LAST_SAVED
-- *          7. MODEL_EXPORT
-- *          8. MODEL_EXTRACT_COMMENT
-- *          9. MODIFIER
-- *          10. OBJECT_PRIORITY_WEIGHTING
-- *          11. EVENT_PRIORITY_WEIGHTING
-- *          12. DEFAULT_OBJECT_PRIORITY
-- *          13. DEFAULT_EVENT_PRIORITY
-- *
-- *  Sorted by ID
-- *
-- *************************************************

CALL UPSERT_VERSION(
'FHNReference',
'3.2.4',
'CURRENT TIMESTAMP',
'CURRENT TIMESTAMP',
'FHNReference',
'2021-02-05 07:01:46.000',
'2021-02-12 13:57:25.929', 
'Script Name:       FHNReference_config_20210212.sql Generated:  '
||'       2021-02-12 13:57:25 CST Model Name:        FHNReference Mo'
||'del FileName:    /FHNReferenceModel/FHNReferenceModel.emx Model V'
||'ersion:     2021-02-05 07:01:46 Extractor Version: 3.2.4.0_v20200'
||'619 Database Type:     DB2 Database Version:  FTM v3.2.4.0-v3.2.4'
||'.x',
'SCRIPT:FHNReference_config_20210212.sql',
0,
0,
0,
0
);


-- *************************************************
-- *
-- *  UPSERT_FORMAT
-- *
-- *  Parameters:
-- *          1. ID
-- *          2. APP_NAME - Application name
-- *          3. APP_VERSION - Application version
-- *          4. DESCRIPTION
-- *          5. MSG_SET
-- *          6. MSG_TYPE
-- *          7. MSG_FORMAT
-- *          8. MSG_DOMAIN
-- *          9. PARSER_OPTIONS
-- *          10. DELETE_FLAG
-- *          11. MODIFIER
-- *
-- *  Sorted by ID
-- *
-- *************************************************

CALL UPSERT_FORMAT (
1000,
'FHNReference',
'3.2.4',
'CBE',
NULL,
NULL, 
NULL, 
'XMLNSC',
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL UPSERT_FORMAT (
1001,
'FHNReference',
'3.2.4',
'ISF Command',
NULL,
NULL, 
NULL, 
'XMLNSC',
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL UPSERT_FORMAT (
1002,
'FHNReference',
'3.2.4',
'ISF',
NULL,
NULL, 
NULL, 
'XMLNSC',
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL UPSERT_FORMAT (
1003,
'FHNReference',
'3.2.4',
'ISF Batch',
NULL,
NULL, 
NULL, 
'XMLNSC',
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL UPSERT_FORMAT (
1005,
'FHNReference',
'3.2.4',
'Pain.001',
NULL,
NULL, 
NULL, 
'XMLNSC',
'',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL UPSERT_FORMAT (
1008,
'FHNReference',
'3.2.4',
'MsgApi',
'PayDirMsgApi',
NULL, 
NULL, 
'XMLNSC',
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL UPSERT_FORMAT (
1009,
'FHNReference',
'3.2.4',
'WTX Blob',
NULL,
NULL, 
NULL, 
'BLOB',
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL UPSERT_FORMAT (
1010,
'FHNReference',
'3.2.4',
'Swift MT',
NULL,
NULL, 
NULL, 
'FXHSPN',
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL UPSERT_FORMAT (
1011,
'FHNReference',
'3.2.4',
'ISO 20022',
NULL,
NULL, 
NULL, 
'XMLNSC',
'RootBitStream, ValidateContent, ValidateValue, ValidateBasicConst'
||'raints, ValidateFullConstraints, ValidateException, XMLNSC.BuildT'
||'reeUsingSchemaTypes',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);


-- *************************************************
-- *
-- *  UPSERT_MAPPER
-- *
-- *  Parameters:
-- *          1. ID
-- *          2. APP_NAME - Application name
-- *          3. APP_VERSION - Application version
-- *          4. FORMAT_ID
-- *          5. ISF_FMT_ID
-- *          6. NAME
-- *          7. DESCRIPTION
-- *          8. INBOUND
-- *          9. PT_TYPE
-- *          10. DELETE_FLAG
-- *          11. MODIFIER
-- *
-- *  Sorted by ID
-- *
-- *************************************************

CALL  UPSERT_MAPPER (
1000,
'FHNReference',
'3.2.4',
1005,
1003,
'PAIN001ToISFMapper',
'PAIN001 to ISF',
'Y',
'PAY_ORIG_BAT', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1004,
'FHNReference',
'3.2.4',
1001,
1001,
'ISFToISFMapper',
'ISF To ISF',
'Y',
'COMMAND', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1005,
'FHNReference',
'3.2.4',
1008,
1002,
'ISFToMsgApiMapper',
'ISF to Messaging API',
'N',
'MESSAGE_API_PT', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1006,
'FHNReference',
'3.2.4',
1008,
1002,
'MsgApiToISFMapper',
'Messaging API to ISF',
'Y',
'MESSAGE_API_PT', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1007,
'FHNReference',
'3.2.4',
1003,
1002,
'ISFToISFOutMapper',
'ISF to ISFOut',
'N',
'EDI_X12', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1008,
'FHNReference',
'3.2.4',
1009,
1002,
'WTX_GEN_IN_MAP',
'Generic inbound WTX integration flows',
'Y',
'EDI_X12', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1009,
'FHNReference',
'3.2.4',
1009,
1002,
'WTX_GEN_OUT_MAP',
'Generic outbound WTX integration flows',
'N',
'EDI_X12', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1010,
'FHNReference',
'3.2.4',
1009,
1002,
'POSTING_OUT_MAP',
'Posting outbound WTX integration flows (separate to allow map sel'
||'ector)',
'N',
'OUT_POSTING_HARD', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1011,
'FHNReference',
'3.2.4',
1010,
1002,
'ISFToMTMapper',
'ISF to MT Mapper',
'N',
'SWIFT_MT', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1012,
'FHNReference',
'3.2.4',
1010,
1002,
'MTToISFMapper',
'MT to ISF',
'Y',
'SWIFT_MT', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1013,
'FHNReference',
'3.2.4',
1009,
1002,
'WTX_GEN_IN_MAP',
'Generic inbound WTX integration flows',
'Y',
'ISO20022', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_MAPPER (
1014,
'FHNReference',
'3.2.4',
1011,
1002,
'ISOMessageRouterInbound',
'Incoming unspecified ISO 20022 message',
'Y',
'CPA_AFT', 
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);


-- *************************************************
-- *
-- *  UPSERT_CALENDAR_GROUP
-- *
-- *  Parameters:
-- *          1. ID
-- *          2. APP_NAME - Application name
-- *          3. APP_VERSION - Application version
-- *          4. NAME - Calendar Group name
-- *          5. DESCRIPTION
-- *          6. TIMEZONE
-- *          7. DELETE_FLAG
-- *          8. MODIFIER - GUI UserId or script name.
-- *
-- *  Sorted by ID
-- *
-- *************************************************

CALL  UPSERT_CALENDAR_GROUP (
1000,
'FHNReference',
'3.2.4',
'Summary Monitor Schedule Group',
' ',
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);


-- *************************************************
-- *
-- *  UPSERT_PARTY
-- *
-- *  Parameters:
-- *          1. ID
-- *          2. APP_NAME - Application name
-- *          3. APP_VERSION - Application version
-- *          4. PARTY_ID
-- *          5. TYPE
-- *          6. REF_ID
-- *          7. NAME
-- *          8. DESCRIPTION
-- *          9. EFF_DATE
-- *          10. END_DATE
-- *          11. DELETE_FLAG
-- *          12. MODIFIER
-- *
-- *  Sorted by ID
-- *
-- *************************************************

CALL  UPSERT_PARTY (
1000,
'FHNReference',
'3.2.4',
NULL, 
'SYSTEM', 
'FTM', 
'FTM', 
'Financial Transaction Manager', 
'2010-06-30 12:14:28.377', 
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_PARTY (
1005,
'FHNReference',
'3.2.4',
NULL, 
'SYSTEM', 
'PD01', 
'Corporate Payments', 
'FTM corporate payments features', 
'9999-12-31 23:59:59.168', 
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_PARTY (
1006,
'FHNReference',
'3.2.4',
NULL, 
'SYSTEM', 
'INT01', 
'Internal Application', 
'Used for all internal file deliveries', 
NULL, 
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_PARTY (
1007,
'FHNReference',
'3.2.4',
NULL, 
'SYSTEM', 
'SFG01', 
'Sterling Gateway', 
'Used for delivery of client files', 
NULL, 
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_PARTY (
2006,
'FHNReference',
'3.2.4',
NULL, 
'SYSTEM', 
'INT01', 
'Internal Application', 
'Used for all internal file deliveries', 
NULL, 
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_PARTY (
2007,
'FHNReference',
'3.2.4',
NULL, 
'SYSTEM', 
'SFG01', 
'Sterling Gateway', 
'Used for delivery of client files', 
NULL, 
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);


-- *************************************************
-- *
-- *  UPSERT_SCHEDULE_ENTRY
-- *
-- *  Parameters:
-- *          1. ID
-- *          2. APP_NAME - Application name
-- *          3. APP_VERSION - Application version
-- *          4. CAL_GROUP_ID
-- *          5. OPEN_TIME
-- *          6. CLOSE_TIME
-- *          7. MON_FLAG
-- *          8. TUE_FLAG
-- *          9. WED_FLAG
-- *          10. THU_FLAG
-- *          11. FRI_FLAG
-- *          12. SAT_FLAG
-- *          13. SUN_FLAG
-- *          14. TYPE_CODE
-- *          15. DELETE_FLAG
-- *          16. MODIFIER - GUI UserId or script name.
-- *  Sorted by ID
-- *
-- *************************************************

CALL  UPSERT_SCHEDULE_ENTRY (
1000,
'FHNReference',
'3.2.4',
1000,
'00:02',
'00:00',
'Y',
'Y',
'Y',
'Y',
'Y',
'Y',
'Y',
'SUMMARY_INTERVAL',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);


-- *************************************************
-- *
-- *  UPSERT_CHANNEL
-- *
-- *  Parameters:
-- *          1. ID
-- *          2. APP_NAME - Application name
-- *          3. APP_VERSION - Application version
-- *          4. PARTY_ID
-- *          5. FORMAT_ID
-- *          6. MAPPER_ID
-- *          7. VALIDATOR_ID
-- *          8. NAME
-- *          9. PARTICIPANT
-- *          10. DESCRIPTION
-- *          11. INBOUND
-- *          12. OPEN
-- *          13. TECH_PRIORITY
-- *          14. LOG_DATA
-- *          15. LOG_TXN
-- *          16. Q_MGR
-- *          17. Q_NAME
-- *          18. SEQUENCE
-- *          19. EFF_DATE
-- *          20. END_DATE
-- *          21. CCSID
-- *          22. ENCODING
-- *          23. VALIDATE_MSG
-- *          24. VALIDATE_ISF
-- *          25. MASTER_FLAG
-- *          26. TRANSPORT
-- *          27. TIMEZONE
-- *          28. LOCATION
-- *          29. PARAM
-- *          30. DELETE_FLAG
-- *          31. MODIFIER - GUI UserId or script name.
-- *  Sorted by ID
-- *
-- *************************************************

CALL  UPSERT_CHANNEL (
1008,
'FHNReference',
'3.2.4',
1007,
1005, 
1000, 
NULL, 
'Client Payment Files', 
NULL, 
'Inbound Client Payment Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'FXH.GPY.PAYMENT.IN', 
50, 
'2010-06-30 12:14:29.096', 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
NULL, 
NULL,
NULL,
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1010,
'FHNReference',
'3.2.4',
1000,
1001, 
1004, 
NULL, 
'Command', 
NULL, 
'Operator Command Messages From FTM Console', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'FXH.GPY.COMMAND', 
50, 
'2010-06-30 12:14:29.143', 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
NULL, 
NULL,
NULL,
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1011,
'FHNReference',
'3.2.4',
1005,
1008, 
1005, 
NULL, 
'Corporate Payments Ingest', 
NULL, 
'Broker tells FTM feature to ingest', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'FXH.GATEWAY.LISTENER.QUEUE', 
NULL, 
NULL, 
NULL, 
1208, 
NULL, 
0, 
0, 
'N', 
NULL, 
NULL,
NULL,
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1012,
'FHNReference',
'3.2.4',
1005,
1008, 
1006, 
NULL, 
'Feature Integration In', 
NULL, 
'FTM features send broker a message', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'FXH.GPY.FEATURE.INTEGRATION', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'N', 
NULL, 
NULL,
NULL,
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1013,
'FHNReference',
'3.2.4',
1006,
1009, 
1009, 
NULL, 
'Bulk CPA', 
NULL, 
'Outbound Bulk CPA Transmissions', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
1148, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/CPA',
'MAPPER_URI=ftmmap://WTX_GEN_OUT_MAP/CPA_Out/CPA_Out,CONFIG=CPA_OU'
||'T,TARGET_DOMAIN=BLOB',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1014,
'FHNReference',
'3.2.4',
1005,
1008, 
1005, 
NULL, 
'Corporate Payments Distribute', 
NULL, 
'Broker updates distribution feature', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'FXH.DISTRIBUTION.INPUT.QUEUE', 
NULL, 
NULL, 
NULL, 
1208, 
NULL, 
0, 
0, 
'N', 
NULL, 
NULL,
NULL,
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1016,
'FHNReference',
'3.2.4',
1006,
1003, 
1007, 
NULL, 
'Bulk Wire', 
NULL, 
'Outbound Bulk Wire Transmissions', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
1208, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/Wire',
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1018,
'FHNReference',
'3.2.4',
1006,
1009, 
1009, 
NULL, 
'Bulk EDI', 
NULL, 
'Outbound Bulk EDI Transmissions', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/EDI',
'MAPPER_URI=ftmmap://WTX_GEN_OUT_MAP/EDI_Out/EDI_X12_Driver_Out,CO'
||'NFIG=EDI_X12_OUT,TARGET_DOMAIN=BLOB',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1019,
'FHNReference',
'3.2.4',
1007,
1009, 
1009, 
NULL, 
'FI Payment File', 
NULL, 
'Outbound FI Payment Transmissions', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/EDIX',
'MAPPER_URI=ftmmap://WTX_GEN_OUT_MAP/EDI_Out/EDI_X12_Driver_Out,CO'
||'NFIG=EDI_X12_OUT,TARGET_DOMAIN=BLOB',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1020,
'FHNReference',
'3.2.4',
1007,
1009, 
1008, 
NULL, 
'EDI Input', 
NULL, 
'Inbound EDI Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
3, 
0, 
'Y', 
'FILE', 
NULL,
'/opt/fhn/apps/ftmapp/iib/EDISource',
'MAPPER_URI=ftmmap://WTX_GEN_IN_MAP/EDI_In/EDI_X12_Driver_In,CONFI'
||'G=EDI_X12_IN,TARGET_DOMAIN=BLOB',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1021,
'FHNReference',
'3.2.4',
1007,
1009, 
1008, 
NULL, 
' CPA Input EBCDIC', 
NULL, 
'Inbound CPA Transmissions in EBCDIC format', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/CPASource_EBCDIC',
'MAPPER_URI=ftmmap://WTX_GEN_IN_MAP/CPA_In/CPA_005_In,CONFIG=CPA_I'
||'N,TARGET_DOMAIN=BLOB',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1022,
'FHNReference',
'3.2.4',
1006,
1009, 
1010, 
NULL, 
'Posting Extract', 
NULL, 
'Outbound Posting Extract Transmissions', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
1148, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/PostingExtracts',
'MAPPER_URI=ftmmap://POSTING_OUT_MAP/Posting_Extracts_Out/Posting_'
||'Out,CONFIG=POSTING_OUT,TARGET_DOMAIN=BLOB',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1031,
'FHNReference',
'3.2.4',
1005,
1009, 
1008, 
NULL, 
'Swift MT Input 101', 
NULL, 
'Inbound Swift MT 101 Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/MT101Source',
'MAPPER_URI=ftmmap://WTX_GEN_IN_MAP/MT101_In/MT101toISFv3,CONFIG=S'
||'WIFT_MT101_IN,TARGET_DOMAIN=XMLNSC',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1032,
'FHNReference',
'3.2.4',
1005,
1009, 
1008, 
NULL, 
'EFT 80 Input', 
NULL, 
'Inbound EFT 80 Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/EFT80Source',
'MAPPER_URI=ftmmap://WTX_GEN_IN_MAP/EFT80_In/EFT80_IN_Driver,CONFI'
||'G=EFT80_IN,TARGET_DOMAIN=XMLNSC',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1033,
'FHNReference',
'3.2.4',
1006,
1010, 
1011, 
NULL, 
'Swift MT Out 103', 
NULL, 
'Outbound Swift MT 103 Transmissions', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/MT',
'CONFIG=SWIFT_MT_OUT,MSG_DEF_SET=fin2013',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1038,
'FHNReference',
'3.2.4',
1006,
1009, 
1009, 
NULL, 
'Swift MT Out 101', 
NULL, 
'Outbound Swift MT 101 Transmissions', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/MT',
'MAPPER_URI=ftmmap://WTX_GEN_OUT_MAP/MT101_Out/ISFv3toMT101,CONFIG'
||'=SWIFT_MT_OUT,TARGET_DOMAIN=BLOB',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1039,
'FHNReference',
'3.2.4',
1005,
1009, 
1008, 
NULL, 
'EFT 1464 Input', 
NULL, 
'Inbound EFT 1464 Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/EFT1464Source',
'MAPPER_URI=ftmmap://WTX_GEN_IN_MAP/EFT1464_In/Maps/CPA_005_In_asc'
||'ii,CONFIG=EFT1464_IN,TARGET_DOMAIN=XMLNSC',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1040,
'FHNReference',
'3.2.4',
1005,
1009, 
1008, 
NULL, 
'Pain 008 Input', 
NULL, 
'Inbound Client Direct Debit Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/Pain008Source',
'MAPPER_URI=ftmmap://WTX_GEN_IN_MAP/Pain008_In/Maps/Pain008_IN_Dri'
||'ver,CONFIG=PAIN008_IN,TARGET_DOMAIN=XMLNSC',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1041,
'FHNReference',
'3.2.4',
1005,
1010, 
1012, 
NULL, 
'Swift MT Input 103', 
NULL, 
'Inbound Swift MT 103 Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
50, 
'2010-06-30 12:14:29.096', 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
NULL, 
NULL,
'/FTMChan/MT103Source',
'CONFIG=SWIFT_MT_IN,MSG_DEF_SET=fin2013',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1043,
'FHNReference',
'3.2.4',
1006,
1009, 
1009, 
NULL, 
'ISO20022 Output', 
NULL, 
'Outbound ISO20022 Transmissions', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/ISO20022',
'CONFIG=ISO20022_OUT,TARGET_DOMAIN=BLOB',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1044,
'FHNReference',
'3.2.4',
1007,
1009, 
1013, 
NULL, 
'ISO20022 Input', 
NULL, 
'Inbound ISO20022 Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
3, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/ISO20022Source',
'CONFIG=ISO20022_IN,MAPPER_CONFIG=ISO20022_IN_MAPPERS,TARGET_DOMAI'
||'N=BLOB',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1045,
'FHNReference',
'3.2.4',
1007,
1009, 
1008, 
NULL, 
'CPA Input ASCII', 
NULL, 
'Inbound CPA Transmissions in ASCII format', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/CPASource_ASCII',
'MAPPER_URI=ftmmap://WTX_GEN_IN_MAP/CPA_In/CPA_005_In,CONFIG=CPA_I'
||'N,TARGET_DOMAIN=BLOB,MAP_CCSID=1148',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1046,
'FHNReference',
'3.2.4',
1007,
1009, 
1008, 
NULL, 
'CPA MQ Input', 
NULL, 
'Inbound CPA MQ Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'FXH.GPY.CPA.IN', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'MQ', 
NULL,
'',
'MAPPER_URI=ftmmap://WTX_GEN_IN_MAP/CPA_In/CPA_005_In,CONFIG=CPA_I'
||'N,TARGET_DOMAIN=BLOB,MAP_CCSID=1148',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1047,
'FHNReference',
'3.2.4',
1000,
1011, 
1014, 
NULL, 
'CPA ISO MQ Input', 
NULL, 
'Inbound CPA ISO MQ Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'Y', 
NULL, 
'FXH.GPY.CPA.ISO.IN', 
50, 
NULL, 
NULL, 
1208, 
NULL, 
0, 
0, 
'Y', 
'MQ', 
NULL,
'',
'CONFIG=CPS_MAP_CFG_REQ_FROM_FI',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
1048,
'FHNReference',
'3.2.4',
1000,
1011, 
1014, 
NULL, 
'CPA ISO Input', 
NULL, 
'Inbound CPA ISO Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'Y', 
NULL, 
'', 
50, 
NULL, 
NULL, 
1208, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/FTMChan/CPASource_ISO',
'CONFIG=CPS_MAP_CFG_REQ_FROM_FI',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
2017,
'FHNReference',
'3.2.4',
2006,
1009, 
1009, 
NULL, 
'Check Printer', 
NULL, 
'Outbound Bulk Check Print Transmissions', 
'N', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
1208, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/opt/fhn/apps/ftmapp/iib/CheckPrint',
'MAPPER_URI=ftmmap://WTX_GEN_OUT_MAP/Checkprint/FHNCCommonOB,CONFI'
||'G=CHECKPRINT_WTX_OUT,TARGET_DOMAIN=BLOB',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
2021,
'FHNReference',
'3.2.4',
2007,
1009, 
1008, 
NULL, 
'CheckPrint Inbound channel', 
NULL, 
'Inbound CheckPrint Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/opt/fhn/apps/ftmapp/iib/CheckPrintSource',
'MAPPER_URI=ftmmap://WTX_GEN_IN_MAP/Bulk_In/FTBBulkInMapper,CONFIG=B'
||'ULK_WTX_IN,TARGET_DOMAIN=XMLNSC',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_CHANNEL (
2022,
'FHNReference',
'3.2.4',
2007,
1009, 
1008, 
NULL, 
'FHNC BulkFile Inbound channel', 
NULL, 
'Inbound BulkFile format Transmissions', 
'Y', 
'Y',
NULL, 
'Y',
'X', 
NULL, 
'', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
0, 
0, 
'Y', 
'FILE', 
NULL,
'/opt/fhn/apps/ftmapp/iib/BulkFormatFileSource',
'MAPPER_URI=ftmmap://WTX_GEN_IN_MAP/Checkprint/FHNCCommon,CONFIG=B'
||'ULK_WTX_IN,TARGET_DOMAIN=XMLNSC',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);


-- *************************************************
-- *
-- *  UPSERT_SERVICE
-- *
-- *  Parameters:
-- *          1. ID
-- *          2. APP_NAME - Application name
-- *          3. APP_VERSION - Application version
-- *          4. NAME
-- *          5. DELETE_FLAG
-- *          6. MODIFIER - GUI UserId or script name.
-- *  Sorted by ID
-- *
-- *************************************************

CALL  UPSERT_SERVICE (
1000,
'FHNReference',
'3.2.4',
'PAYMENT PROCESSING',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_SERVICE (
1001,
'FHNReference',
'3.2.4',
'CPS File Delivery',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);



CALL  UPSERT_SERVICE (
2001,
'FHNReference',
'3.2.4',
'FHNC File Delivery',
'N',
'SCRIPT:FHNReference_config_20210212.sql'
);


-- *************************************************
-- *
-- *  UPSERT_SVC_PARTICIPANT
-- *
-- *  Parameters:
-- *          1. APP_NAME - Application name
-- *          2. APP_VERSION - Application version
-- *          3. SERVICE_ID
-- *          4. CHANNEL_IN_ID
-- *          5. CHANNEL_OUT_ID
-- *          6. NAME
-- *          7. ROLE
-- *          8. RANK
-- *          9. PROC_DATE
-- *          10. OPEN_TIME
-- *          11. CLOSE_TIME
-- *          12. CORREL_SCHEME
-- *          13. DELETE_FLAG
-- *          14. MODIFIER - GUI UserId or script name.
-- *          15. AUDIT_SEQ
-- *          16. TYPE
-- *          17. STATUS
-- *          18. MASTER_FLAG
-- *          19. OWNER_ID
-- *          20. OBJ_CLASS
-- *          21. SUBTYPE
-- *  Sorted by Name
-- *
-- *************************************************

CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1001, 
1021, 
1013, 
'CPA', 
'BULK_CPA_RECEIVER', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1001, 
1045, 
1013, 
'CPA ASCII', 
'BULK_CPA_RECEIVER', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1001, 
1046, 
1013, 
'CPA MQ', 
'BULK_CPA_RECEIVER', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
2001, 
2021, 
2017, 
'Check Print', 
'BULK_CHECK_PRINT_RECEIVER', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1000, 
1012, 
1014, 
'Corporate Payments Distribution', 
'FTM_FEATURE_DIST', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1000, 
NULL, 
1011, 
'Corporate Payments Ingestion', 
'FTM_FEATURE_INGEST', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1001, 
NULL, 
1018, 
'EDI Internal', 
'BULK_EDI_RECEIVER', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
2001, 
2022, 
NULL, 
'FHNC Bulk', 
NULL, 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1001, 
NULL, 
1043, 
'ISO20022', 
'ISO20022_RECEIVER', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
1006,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1000, 
1010, 
NULL, 
'Manual Intervention', 
'OP_COMMAND', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT                     ',
'S_Available                             ',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1000, 
1008, 
NULL, 
'Pain.001 Supplier', 
'BATCHSOURCE', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1001, 
NULL, 
1022, 
'Posting Extracts', 
'BULK_POSTING_RECEIVER', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1001, 
NULL, 
1019, 
'SFG File Delivery', 
'FI_EDI_RECEIVER', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1000, 
1020, 
NULL, 
'SFG File Receipt', 
'BATCHSOURCE', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT                     ',
'S_Available                             ',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1001, 
NULL, 
1038, 
'SWIFT MT 101', 
'SWIFT_MT_RECEIVER_101', 
NULL, 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1001, 
NULL, 
1033, 
'SWIFT MT 103', 
'SWIFT_MT_RECEIVER_103', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);



CALL  UPSERT_SVC_PARTICIPANT (
'FHNReference',
'3.2.4',
1001, 
NULL, 
1016, 
'Wire', 
'BULK_WIRE_RECEIVER', 
'PRIMARY', 
NULL, 
'00:00', 
'23:59',
NULL,
'N', 
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SERVICE_PARTICIPANT',
'S_Available',
'N',
NULL,
'',
''
);


-- *************************************************
-- *
-- *  UPSERT_SCHEDULER_TASK
-- *
-- *  Parameters:
-- *          1. APP_NAME - Application name
-- *          2. APP_VERSION - Application version
-- *          3. NAME
-- *          4. RESOURCE_REF
-- *          5. RESOURCE_REF2
-- *          6. CURRENT_SCHEDULE_ENTRY
-- *          7. TASK_TIME
-- *          8. DELETE_FLAG
-- *          9. MODIFIER - GUI UserId or script name.
-- *          10. AUDIT_SEQ
-- *          11. TYPE
-- *          12. STATUS
-- *          13. MASTER_FLAG
-- *          14. OWNER_ID
-- *          15. OBJ_CLASS
-- *          16. SUBTYPE
-- *  Sorted by NAME
-- *
-- *************************************************

CALL  UPSERT_SCHEDULER_TASK (
'FHNReference',
'3.2.4',
'Batch Payment Originator',
'Batch Payment Originator',
'',
'',
'2000-01-01 00:00:00.000',
'N',
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SCHEDULER_TASK',
'S_MonitorActive',
'N',
0,
NULL,
'HOSTED_SERVICE_MONITOR'
);



CALL  UPSERT_SCHEDULER_TASK (
'FHNReference',
'3.2.4',
'Non STP Monitor',
'',
'',
'',
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql', 
1,
'SCHEDULER_TASK',
'S_MonitorActive',
'N',
0,
NULL,
'NONSTP_SERVICE_MONITOR'
);


-- *************************************************
-- *
-- *  UPSERT_CAL_OBJ_REL
-- *
-- *  Parameters:
-- *          1. OBJ_NAME
-- *          2. OBJ_TYPE
-- *          3. CAL_GROUP_ID
-- *          4. APP_NAME - Application name
-- *          5. APP_VERSION - Application version
-- *          6. DELETE_FLAG
-- *          7. MODIFIER
-- *  Sorted by OBJ_NAME, CAL_GROUP_ID
-- *
-- *************************************************

CALL  UPSERT_CAL_OBJ_REL (
'Batch Payment Originator',
'SCHEDULER_TASK',
1000, 
'FHNReference',
'3.2.4',
'N', 
'SCRIPT:FHNReference_config_20210212.sql'
);


-- *************************************************
-- *
-- *  UPSERT_VALUE
-- *  Parameters:
-- *          1. APP_NAME - Application name
-- *          2. APP_VERSION - Application version
-- *          3. CATEGORY
-- *          4. KEY
-- *          5. VALUE
-- *          6. DELETE_FLAG
-- *          7. MODIFIER - GUI UserId or script name.
-- *  Sorted by CATEGORY, KEY, VALUE
-- *
-- *************************************************

CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'BULK_WTX_IN', 
'POST_BODY_MAPPER', 
'FHNC_POST_MAPPER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'BULK_WTX_IN', 
'PRE_BODY_MAPPER', 
'FHNC_PRE_MAPPER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'BULK_WTX_IN', 
'msgTypeCfg_BULK', 
'<msgTypeCfg><subType>BULK</subType><type>BULK</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CHECKPRINT_WTX_IN', 
'POST_BODY_MAPPER', 
'FHNC_POST_MAPPER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CHECKPRINT_WTX_IN', 
'PRE_BODY_MAPPER', 
'FHNC_PRE_MAPPER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CHECKPRINT_WTX_IN', 
'msgTypeCfg_CHECKPRINT', 
'<msgTypeCfg><subType>CHKP</subType><type>CHKP</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CHECKPRINT_WTX_OUT', 
'msgTypeCfg_CHECKPRINT', 
'<msgTypeCfg><subType>CHKP</subType><bat><subType>Bulk Check Print'
||' Payment Set</subType></bat><pt><subType>Bulk Check Print Payment'
||' Set</subType></pt><mapName>Checkprint/FHNCCommonOB</mapName><typ'
||'e>CHKP</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'COMPLETION_EVENT_FOR_TXN_TYPES', 
'IngestPhysicalTransmission', 
'E_IngestSent',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CORE', 
'IN_FRAG_BAT_GUARD', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CORE', 
'IN_FRAG_PT_GUARD', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CORE', 
'NO_XA', 
'NO_XA',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CORE', 
'PROFILING_FLAGS', 
'MAPPERS=Y MAPDOCS=Y EVENTS=Y TRANSITIONS=Y ACTIONS=Y SQL=Y CUSTOM'
||'=Y PUBLISH=Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CORE', 
'PROFILING_LEVEL', 
'0',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CORE', 
'PROFILING_MAX_SVC_TRACE_SIZE', 
'10',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPA_IN', 
'msgTypeCfg_AFT', 
'<msgTypeCfg><subType>CPA_AFT</subType><bat><subType>CPA_AFT</subT'
||'ype></bat><pt><subType>CPA_AFT</subType></pt><mapName>CPA_005_In<'
||'/mapName><type>AFT</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPA_OUT', 
'BATCH_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPA_OUT', 
'POST_BODY_MAPPER', 
'CPAConvertCCSID',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPA_OUT', 
'PT_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPA_OUT', 
'TXN_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPA_OUT', 
'msgTypeCfg_CPA_OUT', 
'<msgTypeCfg><mapName>CPA_Out</mapName><type>CPA_OUT</type></msgTy'
||'peCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPS_MAP_CFG_REQ_FROM_FI', 
'msgTypeCfg_acmt.022', 
'<msgTypeCfg><class>ACMT022</class><subType>CPS_FROM_DBTR_INSTR</s'
||'ubType><extendedCfg><name>MASTER_FLAG</name><value>Y</value></ext'
||'endedCfg><extendedCfg><name>MAP_FOR_CPS</name><value>Y</value></e'
||'xtendedCfg><pt><class>ACMT022</class><subType>CPA_AFT</subType></'
||'pt><mapName>Acmt022ToISFMapper</mapName><type>acmt.022.001.02</ty'
||'pe></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPS_MAP_CFG_REQ_FROM_FI', 
'msgTypeCfg_pacs.003', 
'<msgTypeCfg><class>PACS003</class><subType>CPS_FROM_CRTR_INSTR</s'
||'ubType><extendedCfg><name>MASTER_FLAG</name><value>Y</value></ext'
||'endedCfg><extendedCfg><name>MAP_FOR_CPS</name><value>Y</value></e'
||'xtendedCfg><pt><class>PACS003</class><subType>CPA_AFT</subType></'
||'pt><mapName>Pacs003ToISFMapper</mapName><type>pacs.003.001.06</ty'
||'pe></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPS_MAP_CFG_REQ_FROM_FI', 
'msgTypeCfg_pacs.004', 
'<msgTypeCfg><class>PACS004</class><subType>CPS_FROM_DBTR_INSTR</s'
||'ubType><extendedCfg><name>MASTER_FLAG</name><value>Y</value></ext'
||'endedCfg><extendedCfg><name>MAP_FOR_CPS</name><value>Y</value></e'
||'xtendedCfg><pt><class>PACS004</class><subType>CPA_AFT</subType></'
||'pt><mapName>Pacs004ToISFMapper</mapName><type>pacs.004.001.06</ty'
||'pe></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPS_MAP_CFG_REQ_FROM_FI', 
'msgTypeCfg_pacs.007', 
'<msgTypeCfg><class>PACS007</class><subType>CPS_FROM_DBTR_INSTR</s'
||'ubType><extendedCfg><name>MASTER_FLAG</name><value>Y</value></ext'
||'endedCfg><extendedCfg><name>MAP_FOR_CPS</name><value>Y</value></e'
||'xtendedCfg><pt><class>PACS008</class><subType>CPA_AFT</subType></'
||'pt><mapName>Pacs007ToISFMapper</mapName><type>pacs.007.001.06</ty'
||'pe></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'CPS_MAP_CFG_REQ_FROM_FI', 
'msgTypeCfg_pacs.008', 
'<msgTypeCfg><class>PACS008</class><subType>CPS_FROM_DBTR_INSTR</s'
||'ubType><extendedCfg><name>MASTER_FLAG</name><value>Y</value></ext'
||'endedCfg><extendedCfg><name>MAP_FOR_CPS</name><value>Y</value></e'
||'xtendedCfg><pt><class>PACS008</class><subType>CPA_AFT</subType></'
||'pt><mapName>Pacs008ToISFMapper</mapName><type>pacs.008.001.06</ty'
||'pe></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'EDI_X12_IN', 
'msgTypeCfg_820', 
'<msgTypeCfg><subType>EDI_X12</subType><bat><subType>EDI_BILL</sub'
||'Type><type>RA</type><txn><subType>EDI_BILL</subType><type>X12_820'
||'</type></txn></bat><bat><subType>EDI_ADVICE</subType><type>AG</ty'
||'pe><txn><subType>EDI_ADVICE</subType><type>X12_824</type></txn></'
||'bat><bat><subType>EDI_ACK</subType><type>FA</type><txn><subType>E'
||'DI_ACK</subType><type>X12_997</type></txn></bat><pt><subType>EDI_'
||'X12</subType></pt><type>EDI_X12</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'EDI_X12_OUT', 
'BATCH_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'EDI_X12_OUT', 
'PT_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'EDI_X12_OUT', 
'TXN_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'EDI_X12_OUT', 
'msgTypeCfg_820', 
'<msgTypeCfg><subType>EDI_X12</subType><bat><subType>OUT_EDI_BILL<'
||'/subType><type>RA</type><txn><subType>OUT_EDI_BILL</subType><type'
||'>X12_820</type></txn></bat><pt><subType>EDI_X12</subType></pt><ty'
||'pe>EDI_X12</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'EFT1464_IN', 
'msgTypeCfg_EFT1464', 
'<msgTypeCfg><subType>CPA_AFT</subType><bat><subType>CPA_AFT</subT'
||'ype><type>EFT1464</type></bat><pt><subType>CPA_AFT</subType></pt>'
||'<mapName>CPA_005_In_ascii</mapName><type>AFT</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'EFT80_IN', 
'msgTypeCfg_EFT80', 
'<msgTypeCfg><subType>EFT80</subType><bat><subType>EFT80</subType>'
||'<type>EFT80</type><txn><subType>EFT80</subType><type>EFT80</type>'
||'</txn></bat><pt><subType>EFT80</subType></pt><type>EFT80</type></'
||'msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'FHNC_REFERENCE', 
'CALC_HASH', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'GUI', 
'FILTERPERIODDEFAULT', 
'3600',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'GUI', 
'LAST10MINS', 
'600',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'GUI', 
'LAST30MINS', 
'1800',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'GUI', 
'LAST5MINS', 
'300',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN', 
'msgTypeCfg_camt.029', 
'<msgTypeCfg><bat><class>OC_XXX</class><subType>ISO20022</subType>'
||'<type>camt.029</type><txn><class>OC_XXX</class><subType>camt.029<'
||'/subType><type>camt.029</type></txn></bat><pt><class>OC_101</clas'
||'s><subType>ISO20022</subType></pt><type>camt.029</type></msgTypeC'
||'fg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN', 
'msgTypeCfg_camt.056', 
'<msgTypeCfg><bat><class>OC_XXX</class><subType>ISO20022</subType>'
||'<type>camt.056</type><txn><class>OC_XXX</class><subType>camt.056<'
||'/subType><type>camt.056</type></txn></bat><pt><class>OC_101</clas'
||'s><subType>ISO20022</subType></pt><type>camt.056</type></msgTypeC'
||'fg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN', 
'msgTypeCfg_pacs.002', 
'<msgTypeCfg><bat><class>OC_XXX</class><subType>ISO20022</subType>'
||'<type>pacs.002.001.06</type><txn><class>OC_XXX</class><subType>pa'
||'cs.002</subType><type>pacs.002.001.06</type></txn></bat><pt><clas'
||'s>OC_101</class><subType>ISO20022</subType></pt><type>pacs.002.00'
||'1.06</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN', 
'msgTypeCfg_pacs.008', 
'<msgTypeCfg><bat><class>OC_XXX</class><subType>ISO20022</subType>'
||'<type>pacs.008.001.05</type><txn><class>OC_XXX</class><subType>pa'
||'cs.008</subType><type>pacs.008.001.05</type></txn></bat><pt><clas'
||'s>OC_101</class><subType>ISO20022</subType></pt><type>pacs.008.00'
||'1.05</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN', 
'msgTypeCfg_pain.001', 
'<msgTypeCfg><bat><class>OC_XXX</class><subType>ISO20022</subType>'
||'<type>pain.001.001.04</type><txn><class>OC_XXX</class><subType>pa'
||'in.001</subType><type>pain.001.001.04</type></txn></bat><pt><clas'
||'s>OC_101</class><subType>ISO20022</subType></pt><type>pain.001.00'
||'1.04</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN', 
'msgTypeCfg_pain.013', 
'<msgTypeCfg><bat><class>OC_XXX</class><subType>ISO20022</subType>'
||'<type>pain.013.001.04</type><txn><class>OC_XXX</class><subType>pa'
||'in.013</subType><type>pain.013.001.04</type></txn></bat><pt><clas'
||'s>OC_101</class><subType>ISO20022</subType></pt><type>pain.013.00'
||'1.04</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN', 
'msgTypeCfg_pain.014', 
'<msgTypeCfg><bat><class>OC_XXX</class><subType>ISO20022</subType>'
||'<type>pain.014.001.04</type><txn><class>OC_XXX</class><subType>pa'
||'in.014</subType><type>pain.014.001.04</type></txn></bat><pt><clas'
||'s>OC_101</class><subType>ISO20022</subType></pt><type>pain.014.00'
||'1.04</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN_MAPPERS', 
'mapperKeyPrefix', 
'mapper_',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN_MAPPERS', 
'mapper_camt.029', 
'ISO20022_In/CAMT029IB',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN_MAPPERS', 
'mapper_camt.056', 
'ISO20022_In/CAMT056IB',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN_MAPPERS', 
'mapper_pacs.002', 
'ISO20022_In/PACS002IB',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN_MAPPERS', 
'mapper_pacs.008', 
'ISO20022_In/PACS008IB',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN_MAPPERS', 
'mapper_pain.001', 
'ISO20022_In/PAIN001IB',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN_MAPPERS', 
'mapper_pain.013', 
'ISO20022_In/PAIN013IB',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN_MAPPERS', 
'mapper_pain.014', 
'ISO20022_In/PAIN014IB',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN_MAPPERS', 
'msgTypeIdEnd', 
'>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_IN_MAPPERS', 
'msgTypeIdStart', 
'Document',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_OUT', 
'BATCH_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_OUT', 
'PT_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_OUT', 
'TXN_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_OUT', 
'msgTypeCfg_camt.029', 
'<msgTypeCfg><subType>camt.029</subType><bat><subType>camt.029</su'
||'bType><type>camt.029</type><txn><subType>camt.029</subType><type>'
||'camt.029</type></txn></bat><pt><subType>camt.029</subType></pt><m'
||'apName>ISO20022_Out/CAMT029OB</mapName><type>camt.029</type></msg'
||'TypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_OUT', 
'msgTypeCfg_camt.056', 
'<msgTypeCfg><subType>camt.056</subType><bat><subType>camt.056</su'
||'bType><type>camt.056</type><txn><subType>camt.056</subType><type>'
||'camt.056</type></txn></bat><pt><subType>camt.056</subType></pt><m'
||'apName>ISO20022_Out/CAMT056OB</mapName><type>camt.056</type></msg'
||'TypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_OUT', 
'msgTypeCfg_pacs.002', 
'<msgTypeCfg><subType>pacs.002</subType><bat><subType>pacs.002</su'
||'bType><type>pacs.002</type><txn><subType>pacs.002</subType><type>'
||'pacs.002</type></txn></bat><pt><subType>pacs.002</subType></pt><m'
||'apName>ISO20022_Out/PACS002OB</mapName><type>pacs.002</type></msg'
||'TypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_OUT', 
'msgTypeCfg_pacs.008', 
'<msgTypeCfg><subType>pacs.008</subType><bat><subType>pacs.008</su'
||'bType><type>pacs.008</type><txn><subType>pacs.008</subType><type>'
||'pacs.008</type></txn></bat><pt><subType>pacs.008</subType></pt><m'
||'apName>ISO20022_Out/PACS008OB</mapName><type>pacs.008</type></msg'
||'TypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_OUT', 
'msgTypeCfg_pain.001', 
'<msgTypeCfg><subType>pain.001</subType><bat><subType>pain.001</su'
||'bType><type>pain.001</type><txn><subType>pain.001</subType><type>'
||'pain.001</type></txn></bat><pt><subType>pain.001</subType></pt><m'
||'apName>ISO20022_Out/PAIN001OB</mapName><type>pain.001</type></msg'
||'TypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_OUT', 
'msgTypeCfg_pain.013', 
'<msgTypeCfg><subType>pain.013</subType><bat><subType>pain.013</su'
||'bType><type>pain.013</type><txn><subType>pain.013</subType><type>'
||'pain.013</type></txn></bat><pt><subType>pain.013</subType></pt><m'
||'apName>ISO20022_Out/PAIN013OB</mapName><type>pain.013</type></msg'
||'TypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ISO20022_OUT', 
'msgTypeCfg_pain.014', 
'<msgTypeCfg><subType>pain.014</subType><bat><subType>pain.014</su'
||'bType><type>pain.014</type><txn><subType>pain.014</subType><type>'
||'pain.014</type></txn></bat><pt><subType>pain.014</subType></pt><m'
||'apName>ISO20022_Out/PAIN014OB</mapName><type>pain.014</type></msg'
||'TypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'MAPPER', 
'POSTING_OUT_MAP_SELECTOR', 
'SELECT ID, CID, UID, OBJ_CLASS, SUBTYPE, ISF_DATA, BANK_CODE, DES'
||'T_BANK_CODE, VALUE_DATE, TXN_DATA1, TXN_DATA2, SETTLEMENT_SYSTEM,'
||' SENDER, RECEIVER, ALT_ID, ACCOUNT, TXN_TIMESTAMP, DEST_ACCOUNT, '
||'CURRENCY, AMOUNT, FX_RATE, DEBIT_CREDIT_FLAG  FROM $DBSchema.TXN_'
||'PAYMENT_V  WHERE BATCH_ID=? WITH UR',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'OBJ_STATUS_COMPLETE', 
'S_InTxnComplete', 
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'OBJ_STATUS_COMPLETE', 
'S_InTxnFailed', 
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'OBJ_STATUS_COMPLETE', 
'S_OutTxnComplete', 
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'OBJ_STATUS_COMPLETE', 
'S_OutTxnFailed', 
NULL,
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'PAIN008_IN', 
'msgTypeCfg_Pain008', 
'<msgTypeCfg><subType>PAIN008</subType><bat><subType>PAIN008</subT'
||'ype><type>PAIN008</type><txn><subType>PAIN008</subType><type>PAIN'
||'008</type></txn></bat><pt><subType>PAIN008</subType></pt><mapName'
||'>Pain008toISFv3</mapName><type>PAIN008</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'POSTING_OUT', 
'BATCH_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'POSTING_OUT', 
'PT_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'POSTING_OUT', 
'TXN_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'POSTING_OUT', 
'msgTypeCfg_HARD', 
'<msgTypeCfg><subType>OUT_POSTING_HARD</subType><type>HARD</type><'
||'/msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'POSTING_OUT', 
'msgTypeCfg_MEMO', 
'<msgTypeCfg><subType>OUT_POSTING_MEMO</subType><type>MEMO</type><'
||'/msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'BULK_CHECK_PRINT', 
'BULK_CHECK_PRINT_RECEIVER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'BULK_CPA', 
'BULK_CPA_RECEIVER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'BULK_EDI', 
'BULK_EDI_RECEIVER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'BULK_WIRE', 
'BULK_WIRE_RECEIVER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'FI_EDI', 
'FI_EDI_RECEIVER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'ISO20022', 
'ISO20022_RECEIVER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'IngestControlTotalTransmission', 
'FTM_FEATURE_INGEST',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'IngestPhysicalTransmission', 
'FTM_FEATURE_INGEST',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'OUT_POSTING_HARD', 
'BULK_POSTING_RECEIVER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'OUT_POSTING_MEMO', 
'BULK_POSTING_RECEIVER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'SWIFT_MT_101', 
'SWIFT_MT_RECEIVER_101',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'SWIFT_MT_103', 
'SWIFT_MT_RECEIVER_103',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'izl_update_outpresgrp', 
'FTM_FEATURE_DIST',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'ROLE_FOR_TXN_TYPE', 
'pacs.008', 
'ISO20022_RECEIVER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT101_IN', 
'msgTypeCfg_MT101', 
'<msgTypeCfg><subType>MT101</subType><bat><subType>MT101</subType>'
||'<type>MT101</type><txn><subType>MT101</subType><type>MT101</type>'
||'</txn></bat><pt><subType>MT101</subType></pt><mapName>MT101toISFv'
||'3</mapName><type>MT101</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_IN', 
'103_POST_BODY_MAPPER', 
'FIN103_POST_MAPPER',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_IN', 
'FIN103', 
'SWIFT_MT103_IN',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_OUT', 
'BATCH_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_OUT', 
'FIN103', 
'PAYMENT_ORIGINATION',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_OUT', 
'PRIORITY', 
'N',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_OUT', 
'PT_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_OUT', 
'RECEIVER', 
'DefDestAddr',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_OUT', 
'SENDER', 
'DefLogclTerm',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_OUT', 
'TXN_OUTMAPPER_OBJVALS', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_OUT', 
'msgTypeCfg_OUT_MT_101', 
'<msgTypeCfg><subType>OUT_MT_101</subType><pt><subType>SWIFT_MT_10'
||'1</subType></pt><type>OUT_MT_101</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'SWIFT_MT_OUT', 
'msgTypeCfg_OUT_MT_103', 
'<msgTypeCfg><subType>OUT_MT_103</subType><pt><subType>SWIFT_MT_10'
||'3</subType></pt><type>OUT_MT_103</type></msgTypeCfg>',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'WTX_INTEGRATION', 
'NUMBER_ERROR_RECS', 
'5',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);



CALL  UPSERT_VALUE (
'FHNReference',
'3.2.4',
'WTX_INTEGRATION', 
'SERIALIZE_ALL', 
'Y',
'N',
'SCRIPT:FHNReference_config_20210212.sql',
''
);


-- *************************************************
-- *
-- *  UPSERT_CLASSIFICATION
-- *
-- *  Parameters:
-- *          1. ID
-- *          2. APP_NAME - Application name
-- *          3. APP_VERSION - Application version
-- *          4. SCHEME
-- *          5. CODE
-- *          6. DESCRIPTION
-- *          7. SEQUENCE
-- *          8. EFF_DATE
-- *          9. END_DATE
-- *          10 .DELETE_FLAG
-- *          11. MODIFIER - GUI UserId or script name.
-- *  Sorted by SCHEME, CODE, DESCRIPTION
-- *
-- *************************************************

CALL  UPSERT_CLASSIFICATION (
1, 
'FHNReference',
'3.2.4',
'CCSID', 
'1208', 
'UTF-8', 
50, 
'2010-06-30 12:14:34.752', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
2, 
'FHNReference',
'3.2.4',
'CURRENCY', 
'AUD', 
'Australia Dollars', 
50, 
'2010-06-30 12:14:35.174', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
3, 
'FHNReference',
'3.2.4',
'CURRENCY', 
'CAD', 
'Canada Dollars', 
50, 
'2010-06-30 12:14:35.174', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
4, 
'FHNReference',
'3.2.4',
'CURRENCY', 
'CHF', 
'Switzerland Francs', 
50, 
'2010-06-30 12:14:35.190', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
5, 
'FHNReference',
'3.2.4',
'CURRENCY', 
'EUR', 
'Euro', 
50, 
'2010-06-30 12:14:35.205', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
6, 
'FHNReference',
'3.2.4',
'CURRENCY', 
'GBP', 
'United Kingdom Pounds', 
50, 
'2010-06-30 12:14:35.205', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
7, 
'FHNReference',
'3.2.4',
'CURRENCY', 
'HKD', 
'Hong Kong Dollars', 
50, 
'2010-06-30 12:14:35.221', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
8, 
'FHNReference',
'3.2.4',
'CURRENCY', 
'JPY', 
'Japan Yen', 
50, 
'2010-06-30 12:14:35.221', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
9, 
'FHNReference',
'3.2.4',
'CURRENCY', 
'USD', 
'United States Dollars', 
50, 
'2010-06-30 12:14:35.237', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
10, 
'FHNReference',
'3.2.4',
'DC_FLAG', 
'C', 
'C', 
50, 
'2010-06-30 12:14:34.768', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
11, 
'FHNReference',
'3.2.4',
'DC_FLAG', 
'D', 
'D', 
50, 
'2010-06-30 12:14:34.784', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
12, 
'FHNReference',
'3.2.4',
'DEBIT_CREDIT_FLAG', 
'C', 
'Credit', 
50, 
'2010-06-30 12:14:34.799', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
13, 
'FHNReference',
'3.2.4',
'DEBIT_CREDIT_FLAG', 
'D', 
'Debit', 
50, 
'2010-06-30 12:14:34.799', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
14, 
'FHNReference',
'3.2.4',
'DELETEFLAG', 
'N', 
'No', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
15, 
'FHNReference',
'3.2.4',
'DELETEFLAG', 
'P', 
'Unknown', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
16, 
'FHNReference',
'3.2.4',
'DELETEFLAG', 
'X', 
'Yes', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
17, 
'FHNReference',
'3.2.4',
'ERRTYPE', 
'ERRPARSE', 
'Parse Error', 
50, 
'2010-06-30 12:14:35.252', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
18, 
'FHNReference',
'3.2.4',
'ERRTYPE', 
'ERRSYS', 
'System Error', 
50, 
'2010-06-30 12:14:35.252', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
19, 
'FHNReference',
'3.2.4',
'ERRTYPE', 
'ERRVALID', 
'Validation Error', 
50, 
'2010-06-30 12:14:35.268', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
20, 
'FHNReference',
'3.2.4',
'EVTPRI', 
'10', 
'Low', 
50, 
'2010-06-30 12:14:35.268', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
21, 
'FHNReference',
'3.2.4',
'EVTPRI', 
'50', 
'Medium', 
50, 
'2010-06-30 12:14:35.284', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
22, 
'FHNReference',
'3.2.4',
'EVTPRI', 
'70', 
'High', 
50, 
'2010-06-30 12:14:35.299', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
23, 
'FHNReference',
'3.2.4',
'EVTSEV', 
'0', 
'Unknown', 
50, 
'2010-06-30 12:14:35.299', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
24, 
'FHNReference',
'3.2.4',
'EVTSEV', 
'10', 
'Information', 
50, 
'2010-06-30 12:14:35.315', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
25, 
'FHNReference',
'3.2.4',
'EVTSEV', 
'20', 
'Harmless', 
50, 
'2010-06-30 12:14:35.315', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
26, 
'FHNReference',
'3.2.4',
'EVTSEV', 
'30', 
'Warning', 
50, 
'2010-06-30 12:14:35.330', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
27, 
'FHNReference',
'3.2.4',
'EVTSEV', 
'40', 
'Minor', 
50, 
'2010-06-30 12:14:35.330', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
28, 
'FHNReference',
'3.2.4',
'EVTSEV', 
'50', 
'Critical', 
50, 
'2010-06-30 12:14:35.346', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
29, 
'FHNReference',
'3.2.4',
'EVTSEV', 
'60', 
'Fatal', 
50, 
'2010-06-30 12:14:35.362', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
32, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_AckRecvd', 
'Acknowledgment Received', 
50, 
'2010-06-30 12:14:35.409', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
33, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_BatInTxnComplete', 
'Inbound Batch Transaction Complete', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
34, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_BatOutTxnComplete', 
'Outbound Batch Transaction Complete', 
50, 
'2010-06-30 12:14:35.424', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
35, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_BatTxnValComplete', 
'Batch Transaction Validation Complete', 
50, 
'2010-06-30 12:14:35.424', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
36, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_BatchValFail', 
'Batch Header Validation Failure', 
50, 
'2010-06-30 12:14:35.440', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
37, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_BatchValPass', 
'Batch Header Validation Passed', 
50, 
'2010-06-30 12:14:35.455', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
38, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_BatchValid', 
'Batch Header Validated', 
50, 
'2010-06-30 12:14:35.455', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
39, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_CmdAuthRejected', 
'Command Authorization Rejected', 
50, 
'2010-06-30 12:14:35.471', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
40, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_CmdAuthorityRequired', 
'Command Authorization Required', 
50, 
'2010-06-30 12:14:35.487', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
41, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_CmdAuthorized', 
'Command Authorized', 
50, 
'2010-06-30 12:14:35.487', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
42, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_CmdError', 
'Command Error', 
50, 
'2010-06-30 12:14:35.502', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
43, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_Heartbeat', 
'Heartbeat', 
50, 
'2010-06-30 12:14:35.502', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
44, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_HeartbeatStart', 
'Heartbeat Start', 
50, 
'2010-06-30 12:14:35.518', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
45, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_InBatComplete', 
'Inbound Batch Completed', 
50, 
'2010-06-30 12:14:35.534', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
46, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_InBatFailed', 
'Inbound Batch Failed', 
50, 
'2010-06-30 12:14:35.534', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
47, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_InPTAllChildrenComplete', 
'Inbound Transmission Children Complete', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
48, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_InPTComplete', 
'Inbound Transmission Complete', 
50, 
'2010-06-30 12:14:35.549', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
49, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_InPTFailed', 
'Inbound Transmission Failed', 
50, 
'2010-06-30 12:14:35.565', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
50, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_InTxnComplete', 
'Inbound Transaction Complete', 
50, 
'2010-06-30 12:14:35.565', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
51, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_InTxnFailed', 
'Inbound Transaction Failed', 
50, 
'2010-06-30 12:14:35.580', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
30, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_IngestSent', 
'Corporate Payments ingest request sent', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
52, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_MpInBatMapFailure', 
'Batch Mapping Failure', 
50, 
'2010-06-30 12:14:35.596', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
53, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_MpInBatMapped', 
'Batch Mapped', 
50, 
'2010-06-30 12:14:35.596', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
54, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_MpInMappingAborted', 
'Mapping Aborted', 
50, 
'2010-06-30 12:14:35.612', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
55, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_MpInPTMapped', 
'Inbound Transmission Mapped', 
50, 
'2010-06-30 12:14:35.627', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
56, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_MpInTxnMapFailure', 
'Inbound Transaction Mapping Failure', 
50, 
'2010-06-30 12:14:35.627', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
57, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_MpInTxnMapped', 
'Inbound Transaction Mapped', 
50, 
'2010-06-30 12:14:35.643', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
58, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_NoCmdAuthorityRequired', 
'Command Authorization Not Required', 
50, 
'2010-06-30 12:14:35.659', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
59, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_NotifyOps', 
'Notify Operations (Alert)', 
50, 
'2010-06-30 12:14:36.424', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
60, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OperatorCancel', 
'Operator Cancellation', 
50, 
'2010-06-30 12:14:35.659', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
61, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OperatorContinue', 
'Operator Continue', 
50, 
'2010-06-30 12:14:35.674', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
62, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OperatorVerify', 
'Operator Verification', 
50, 
'2010-06-30 12:14:35.674', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
63, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutBatComplete', 
'Outbound Batch Complete', 
50, 
'2010-06-30 12:14:35.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
64, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutBatFailed', 
'Outbound Batch Failed', 
50, 
'2010-06-30 12:14:35.705', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
65, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutPTFailed', 
'Outbound Transmission Failed', 
50, 
'2010-06-30 12:14:35.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
66, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutPTResend', 
'Outbound Transmission Resend', 
50, 
'2010-06-30 12:14:35.705', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
31, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutPTRouteFailed', 
'Outbound Transmission Route Failed', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
67, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutPTSendConfirmed', 
'Outbound Transmission Send Confirmed', 
50, 
'2010-06-30 12:14:35.721', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
68, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutPTSendFailed', 
'Outbound Transmission Send Failed', 
50, 
'2010-06-30 12:14:35.737', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
69, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutPTSent', 
'Outbound Transmission Sent', 
50, 
'2010-06-30 12:14:35.737', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
70, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutTxnComplete', 
'Outbound Transaction Complete', 
50, 
'2010-06-30 12:14:35.752', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
71, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutTxnFailed', 
'Outbound Transaction Failed', 
50, 
'2010-06-30 12:14:35.752', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
72, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutTxnRouteFailed', 
'Outbound Transaction Routing Failed', 
50, 
'2010-06-30 12:14:35.768', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
73, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutTxnRouted', 
'Outbound Transaction Routed', 
50, 
'2010-06-30 12:14:35.768', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
74, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutTxnSent', 
'Outbound Transaction Sent', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
75, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_OutTxnWaitForAck', 
'Outbound Transaction Wait for Acknowldgement', 
50, 
'2010-06-30 12:14:35.784', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
76, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_PTDupFail', 
'Duplicate Transmission', 
50, 
'2010-06-30 12:14:35.784', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
77, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_PTOutCreated', 
'Outbound Physical Transmission Created', 
50, 
'2010-06-30 12:14:35.799', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
78, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_PTOutMapAborted', 
'Outbound Physical Transmission Mapping Aborted', 
50, 
'2010-06-30 12:14:35.799', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
79, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_PTValFail', 
'Transmission Validation Failed', 
50, 
'2010-06-30 12:14:35.815', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
80, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_PTValid', 
'Transmission validated', 
50, 
'2010-06-30 12:14:35.815', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
81, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_RelUpdatingFail', 
'Relationships Update Failed', 
50, 
'2010-06-30 12:14:35.830', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
82, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_TimeoutNotify', 
'Timeout Notify Operations (Alert)', 
50, 
'2010-06-30 12:14:36.424', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
83, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_TxnBatchValid', 
'Transactions Batch Valid', 
50, 
'2010-06-30 12:14:35.846', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
84, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_TxnOutCreated', 
'Outbound Transaction Created', 
50, 
'2010-06-30 12:14:35.846', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
85, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_TxnOutTxnComplete', 
'Transactions Outbound Transaction Complete', 
50, 
'2010-06-30 12:14:35.862', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
86, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_UnexpectedError', 
'Unexpected Error', 
50, 
'2010-06-30 12:14:35.862', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
87, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_ValFail', 
'Validation Failed', 
50, 
'2010-06-30 12:14:35.877', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
88, 
'FHNReference',
'3.2.4',
'EVTTYPE', 
'E_ValPass', 
'Validation Passed', 
50, 
'2010-06-30 12:14:35.877', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
89, 
'FHNReference',
'3.2.4',
'FILTERPERIOD', 
'ASSPECIFIED', 
'Between...', 
10, 
'2010-06-30 12:14:34.815', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
90, 
'FHNReference',
'3.2.4',
'FILTERPERIOD', 
'FILTERPERIODDEFAULT', 
'Last 60 Minutes', 
50, 
'2010-06-30 12:14:34.846', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
91, 
'FHNReference',
'3.2.4',
'FILTERPERIOD', 
'LAST10MINS', 
'Last 10 Minutes', 
30, 
'2010-06-30 12:14:34.877', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
92, 
'FHNReference',
'3.2.4',
'FILTERPERIOD', 
'LAST30MINS', 
'Last 30 Minutes', 
30, 
'2010-06-30 12:14:34.862', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
93, 
'FHNReference',
'3.2.4',
'FILTERPERIOD', 
'LAST5MINS', 
'Last 5 Minutes', 
20, 
'2010-06-30 12:14:34.909', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
94, 
'FHNReference',
'3.2.4',
'FILTERPERIOD', 
'TODAY', 
'Today', 
1000, 
'2010-06-30 12:14:34.909', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
95, 
'FHNReference',
'3.2.4',
'MASTERFLAG', 
'N', 
'No', 
50, 
'2010-06-30 12:14:34.924', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
96, 
'FHNReference',
'3.2.4',
'MASTERFLAG', 
'Y', 
'Yes', 
50, 
'2010-06-30 12:14:34.940', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
97, 
'FHNReference',
'3.2.4',
'MSG_VALIDATION', 
'0', 
'None', 
50, 
'2010-06-30 12:14:34.955', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
98, 
'FHNReference',
'3.2.4',
'MSG_VALIDATION', 
'1', 
'Content', 
50, 
'2010-06-30 12:14:34.971', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
99, 
'FHNReference',
'3.2.4',
'MSG_VALIDATION', 
'2', 
'Content and Value', 
50, 
'2010-06-30 12:14:34.971', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
100, 
'FHNReference',
'3.2.4',
'MSG_VALIDATION', 
'3', 
'Content, value and full constraints', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
101, 
'FHNReference',
'3.2.4',
'OBJREL', 
'ACK_TO', 
'Acknowledgment to', 
50, 
'2010-06-30 12:14:35.893', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
102, 
'FHNReference',
'3.2.4',
'OBJREL', 
'CAUSE', 
'Cause of', 
50, 
'2010-06-30 12:14:35.909', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
103, 
'FHNReference',
'3.2.4',
'OBJREL', 
'CONTAINS', 
'Contains', 
50, 
'2010-06-30 12:14:35.909', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
104, 
'FHNReference',
'3.2.4',
'OBJREL', 
'RESPONSE', 
'Response to', 
50, 
'2010-06-30 12:14:35.924', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
105, 
'FHNReference',
'3.2.4',
'OBJREL', 
'TARGETS', 
'Targets', 
50, 
'2010-06-30 12:14:35.924', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
109, 
'FHNReference',
'3.2.4',
'OBJSTATUS_BAT', 
'S_BatMapped', 
'Batch Mapped', 
50, 
'2010-06-30 12:14:36.424', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
110, 
'FHNReference',
'3.2.4',
'OBJSTATUS_BAT', 
'S_BatValid', 
'Batch Valid', 
50, 
'2010-06-30 12:14:36.440', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
111, 
'FHNReference',
'3.2.4',
'OBJSTATUS_BAT', 
'S_InBatComplete', 
'Batch Complete', 
50, 
'2010-06-30 12:14:36.471', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
106, 
'FHNReference',
'3.2.4',
'OBJSTATUS_BAT', 
'S_OutBatComplete', 
'Outbound Batch Complete', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
107, 
'FHNReference',
'3.2.4',
'OBJSTATUS_BAT', 
'S_OutBatCreated', 
'Outbound Batch Created', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
108, 
'FHNReference',
'3.2.4',
'OBJSTATUS_BAT', 
'S_OutBatDeleted', 
'Outbound Batch Deleted', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
113, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_InPTArrived', 
'Inbound Transmission Arrived', 
50, 
'2010-06-30 12:14:35.924', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
114, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_InPTComplete', 
'Inbound Transmission Complete', 
50, 
'2010-06-30 12:14:35.940', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
115, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_InPTFailed', 
'Inbound Transmission Failed', 
50, 
'2010-06-30 12:14:35.940', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
116, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_InPTMapFailed', 
'Inbound Transmission Mapping Failed', 
50, 
'2010-06-30 12:14:35.955', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
117, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_InPTProcessing', 
'In Process', 
50, 
'2010-06-30 12:14:35.955', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
118, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_InPTValidating', 
'In Validating', 
50, 
'2010-06-30 12:14:35.971', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
119, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_OutPTAwaitingSend', 
'Awaiting Transmission', 
50, 
'2010-06-30 12:14:35.971', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
120, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_OutPTCreated', 
'Outbound Transmission Created', 
50, 
'2010-06-30 12:14:35.987', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
112, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_OutPTDeleted', 
'Outbound Transmission Deleted', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
121, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_OutPTDeliveryError', 
'Transmission Delivery Error', 
50, 
'2010-06-30 12:14:35.987', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
122, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_OutPTFailed', 
'Outbound Transmission Failed', 
50, 
'2010-06-30 12:14:36.002', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
123, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_OutPTSendConfirmed', 
'Transmission Send Confirmed', 
50, 
'2010-06-30 12:14:36.002', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
124, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_OutPTSendError', 
'Transmission Send Error', 
50, 
'2010-06-30 12:14:36.018', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
125, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_OutPTSent', 
'Transmission Sent', 
50, 
'2010-06-30 12:14:36.018', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
126, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_OutPTWaitingOpsVerify', 
'Awaiting Operator Verification', 
50, 
'2010-06-30 12:14:36.034', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
127, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_PTDupFail', 
'Duplicate Transmission', 
50, 
'2010-06-30 12:14:36.127', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
128, 
'FHNReference',
'3.2.4',
'OBJSTATUS_PT', 
'S_PTValidationFail', 
'Transmission Validation Failed', 
50, 
'2010-06-30 12:14:36.143', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
134, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_AuthorizePending', 
'Authorization Pending', 
50, 
'2010-06-30 12:14:36.049', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
135, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_CheckingAuthorityValidating', 
'Checking Authority', 
50, 
'2010-06-30 12:14:36.065', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
136, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_InTxnComplete', 
'Inbound Transaction Complete', 
50, 
'2010-06-30 12:14:36.065', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
137, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_InTxnFailed', 
'Transaction Failed', 
50, 
'2010-06-30 12:14:36.080', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
138, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_InTxnWaitingOpsVerify', 
'Awaiting Operator Verification', 
50, 
'2010-06-30 12:14:36.080', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
139, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_OutTxnAwaitingSend', 
'Awaiting Transmission', 
50, 
'2010-06-30 12:14:36.080', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
140, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_OutTxnComplete', 
'Outbound Transaction Complete', 
50, 
'2010-06-30 12:14:36.096', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
141, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_OutTxnCreated', 
'Outbound Transaction Created', 
50, 
'2010-06-30 12:14:36.096', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
129, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_OutTxnDeleted', 
'Outbound Transaction Deleted', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
142, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_OutTxnFailed', 
'Outbound Transaction Failed', 
50, 
'2010-06-30 12:14:36.112', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
143, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_OutTxnRouteError', 
'Transaction Routing Error', 
50, 
'2010-06-30 12:14:36.034', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
144, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_ProcessingCommand', 
'Processing Command', 
50, 
'2010-06-30 12:14:36.127', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
145, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_RelUpdateFailed', 
'Correlation Failed', 
50, 
'2010-06-30 12:14:36.159', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
146, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_RelUpdating', 
'Correlating', 
50, 
'2010-06-30 12:14:36.143', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
130, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_TxnDistAwaitingSend', 
'Distribution Sending', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
131, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_TxnMapped', 
'Transaction Mapped', 
50, 
'2010-06-30 12:14:36.159', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
132, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_TxnSendDistFailed', 
'Distribution Send Failed', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
133, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_TxnSentDist', 
'Distribution Sent', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
147, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_WaitOutBatComplete', 
'Wait for out Batch Completion', 
50, 
'2010-06-30 12:14:36.190', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
148, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_WaitingForAck', 
'Waiting for Response', 
50, 
'2010-06-30 12:14:36.174', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
149, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_WaitingForAckTimeout', 
'Timeout Waiting for Response', 
50, 
'2010-06-30 12:14:36.174', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
150, 
'FHNReference',
'3.2.4',
'OBJSTATUS_TXN', 
'S_WaitingForFinalAck', 
'Waiting for Final Ack', 
50, 
'2010-06-30 12:14:36.190', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
151, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'CPA_AFT', 
'CPA AFT', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
152, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'EDI_ACK', 
'EDI Functional Acknowledgments', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
153, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'EDI_ADVICE', 
'EDI Application Advices', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
154, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'EDI_BILL', 
'EDI Bill Payments', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
155, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'EDI_CANCEL', 
'EDI Cancellation Requests', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
156, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'EDI_RETURN', 
'EDI Returns', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
157, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'EDI_X12', 
'EDI X12', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
168, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'EFT80', 
'EFT80 Byte File Credits and Debits', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
169, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'MT101', 
'MT101 Request for transfer', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
158, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'NACHA_XCK', 
'NACHA XCK', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
159, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_CPA_AFT', 
'Outbound CPA AFT', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
160, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_EDI_ACK', 
'Outbound EDI Functional Acknowledgments', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
161, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_EDI_ADVICE', 
'Outbound EDI Application Advices', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
162, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_EDI_BILL', 
'Outbound EDI Bill Payments', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
163, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_EDI_RETURN', 
'Outbound EDI Returns', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
170, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_MT_101', 
'Outgoing MT101 Request for Transfer', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
171, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_MT_103', 
'Outgoing MT103 Credit Transfer', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
164, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_NACHA_CTX', 
'Outbound NACHA CTX', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
165, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_NACHA_XCK', 
'Outbound NACHA XCK', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
166, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_POSTING_HARD', 
'Outbound Hard Postings', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
167, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'OUT_POSTING_MEMO', 
'Outbound Memo Postings', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
172, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'PAIN008', 
'PAIN 008 Direct Debit', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
173, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_BAT', 
'PAYMENT_ORIGINATION', 
'Payment Origination', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
174, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'BULK_CHECK_PRINT', 
'Bulk Check Print Payment Set', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
175, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'BULK_CPA', 
'Bulk CPA Payment Set', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
176, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'BULK_EDI', 
'Bulk EDI Payment Set', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
177, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'BULK_WIRE', 
'Bulk Wire Payment Set', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
178, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'COMMAND', 
'Operator Command', 
50, 
'2010-06-30 12:14:36.315', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
179, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'CPA_AFT', 
'CPA AFT', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
180, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'EDI_X12', 
'EDI X12', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
181, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'EFT80', 
'EFT80 Byte File Credits and Debits', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
182, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'FI_EDI', 
'FI EDI Payment Set', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
183, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'ISO20022', 
'ISO 20022', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
184, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'MESSAGE_API_PT', 
'Messaging API Transmission', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
185, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'MT101', 
'MT101 Request for transfer', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
186, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'OUT_MT_101', 
'Outgoing MT101 Request for Transfer', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
187, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'OUT_MT_103', 
'Outgoing MT103 Credit Transfer', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
188, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'OUT_POSTING_HARD', 
'Outbound Hard Postings', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
189, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'OUT_POSTING_MEMO', 
'Outbound Memo Postings', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
190, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'PAIN008', 
'PAIN 008 Direct Debit', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
191, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'PAY_ORIG_BAT', 
'Payment Origination Batch', 
50, 
'2010-06-30 12:14:36.549', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
192, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_PT', 
'SWIFT_MT', 
'SWIFT MT', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
214, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'COMMAND', 
'Operator Command', 
50, 
'2010-06-30 12:14:36.315', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
193, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'CONTROL_TOTAL', 
'Control Totals', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
194, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'CPA_AFT', 
'CPA AFT', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
195, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'EDI_ACK', 
'EDI Functional Acknowledgments', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
196, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'EDI_ADVICE', 
'EDI Application Advices', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
197, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'EDI_BILL', 
'EDI Bill Payments', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
198, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'EDI_CANCEL', 
'EDI Cancellation Requests', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
199, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'EDI_RETURN', 
'EDI Returns', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
215, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'EFT80', 
'EFT80 Byte File Credits and Debits', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
200, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'IngestControlTotalTransmission', 
'Ingest Control Total Transmission', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
201, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'IngestPhysicalTransmission', 
'Ingest Transmission', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
216, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'MT101', 
'MT101 Request for transfer', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
217, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'MT103', 
'MT103 Single Customer Credit Transfer', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
218, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'MT104', 
'MT104 Direct Debit Instruction', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
202, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'NACHA_XCK', 
'NACHA XCK', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
203, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_CPA_AFT', 
'Outbound CPA AFT', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
204, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_EDI_ACK', 
'Outbound EDI Functional Acknowledgments', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
205, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_EDI_ADVICE', 
'Outbound EDI Application Advices', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
206, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_EDI_BILL', 
'Outbound EDI Bill Payments', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
207, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_EDI_RETURN', 
'Outbound EDI Returns', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
219, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_MT_101', 
'Outgoing MT101 Request for Transfer', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
220, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_MT_103', 
'Outgoing MT103 Credit Transfer', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
208, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_NACHA_CTX', 
'Outbound NACHA CTX', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
209, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_NACHA_XCK', 
'Outbound NACHA XCK', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
210, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_POSTING_HARD', 
'Outbound Hard Postings', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
211, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'OUT_POSTING_MEMO', 
'Outbound Memo Postings', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
221, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'PAIN008', 
'PAIN 008 Direct Debit', 
50, 
'2010-06-30 12:14:36.690', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
222, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'PAYMENT_INS', 
'Payment Instruction', 
50, 
'2010-06-30 12:14:36.659', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
223, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'PAYMENT_ORIGINATION', 
'Payment Origination', 
50, 
'2010-06-30 12:14:36.659', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
212, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'ReleasePhysicalTransmission', 
'Release Transmission', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
213, 
'FHNReference',
'3.2.4',
'OBJSUBTYPE_TXN', 
'izl_update_outpresgrp', 
'Update Distribution', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
224, 
'FHNReference',
'3.2.4',
'OBJTYPE', 
'ACTIVITY', 
'Activity', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
225, 
'FHNReference',
'3.2.4',
'OBJTYPE', 
'BATCH', 
'Batch', 
50, 
'2010-06-30 12:14:34.987', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
226, 
'FHNReference',
'3.2.4',
'OBJTYPE', 
'FRAGMENT', 
'Fragment', 
50, 
'2010-06-30 12:14:35.002', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
227, 
'FHNReference',
'3.2.4',
'OBJTYPE', 
'SCHEDULER_TASK', 
'Scheduler Task', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
228, 
'FHNReference',
'3.2.4',
'OBJTYPE', 
'SERVICE_PARTICIPANT', 
'Service Participant', 
50, 
'2010-06-30 12:14:35.034', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
229, 
'FHNReference',
'3.2.4',
'OBJTYPE', 
'TRANSACTION', 
'Transaction', 
50, 
'2010-06-30 12:14:35.034', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
230, 
'FHNReference',
'3.2.4',
'OBJTYPE', 
'TRANSMISSION', 
'Physical Transmission', 
50, 
'2010-06-30 12:14:35.049', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
231, 
'FHNReference',
'3.2.4',
'OBJTYPE', 
'TXN_PAYMENT', 
'Payment Transaction', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
232, 
'FHNReference',
'3.2.4',
'OBJTYPE', 
'TXN_SECURITIES', 
'Securities Transaction', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
233, 
'FHNReference',
'3.2.4',
'PARTYTYPE', 
'SYSTEM', 
'System', 
50, 
'2010-06-30 12:14:36.205', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
234, 
'FHNReference',
'3.2.4',
'PAYMETCODE', 
'SWT', 
'Swift', 
50, 
'2010-06-30 12:14:36.205', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
235, 
'FHNReference',
'3.2.4',
'PAYTYPE', 
'CPA_AFT', 
'CPA AFT', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
241, 
'FHNReference',
'3.2.4',
'PAYTYPE', 
'WIR', 
'Wire', 
50, 
'2010-06-30 12:14:36.221', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
236, 
'FHNReference',
'3.2.4',
'PAYTYPE', 
'X12_820', 
'EDI X12 820', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
237, 
'FHNReference',
'3.2.4',
'PAYTYPE', 
'X12_824', 
'EDI X12 824', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
238, 
'FHNReference',
'3.2.4',
'PAYTYPE', 
'X12_827', 
'EDI X12 827', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
239, 
'FHNReference',
'3.2.4',
'PAYTYPE', 
'X12_829', 
'EDI X12 829', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
240, 
'FHNReference',
'3.2.4',
'PAYTYPE', 
'X12_997', 
'EDI X12 997', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
242, 
'FHNReference',
'3.2.4',
'PREF_DEFAULT_PAGE', 
'all_alerts.jsp', 
'Alerts', 
50, 
'2010-08-20 16:17:18.828', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
243, 
'FHNReference',
'3.2.4',
'PREF_DEFAULT_PAGE', 
'preferences.jsp', 
'Preferences', 
50, 
'2010-08-20 16:17:18.843', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
244, 
'FHNReference',
'3.2.4',
'PREF_DEFAULT_PAGE', 
'welcome.jsp', 
'Welcome', 
50, 
'2010-08-20 16:17:18.843', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
245, 
'FHNReference',
'3.2.4',
'PREF_LIST_ROWSPAGE', 
'10', 
'10', 
50, 
'2010-08-20 16:17:18.843', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
246, 
'FHNReference',
'3.2.4',
'PREF_LIST_ROWSPAGE', 
'100', 
'100', 
50, 
'2010-08-20 16:17:18.843', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
247, 
'FHNReference',
'3.2.4',
'PREF_LIST_ROWSPAGE', 
'20', 
'20', 
50, 
'2010-08-20 16:17:18.843', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
248, 
'FHNReference',
'3.2.4',
'PREF_LIST_ROWSPAGE', 
'25', 
'25', 
50, 
'2010-08-20 16:17:18.843', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
249, 
'FHNReference',
'3.2.4',
'PREF_LIST_ROWSPAGE', 
'50', 
'50', 
50, 
'2010-08-20 16:17:18.843', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
250, 
'FHNReference',
'3.2.4',
'PREF_LOCALE', 
'en_GB', 
'English', 
50, 
'2010-08-20 16:17:18.859', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
251, 
'FHNReference',
'3.2.4',
'PREF_LOCALE', 
'en_US', 
'English US', 
50, 
'2010-08-20 16:17:18.859', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
252, 
'FHNReference',
'3.2.4',
'PREF_LOCALE', 
'es', 
'Spanish', 
50, 
'2010-08-20 16:17:18.859', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
253, 
'FHNReference',
'3.2.4',
'PREF_LOCALE', 
'fr', 
'French', 
50, 
'2010-08-20 16:17:18.859', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
254, 
'FHNReference',
'3.2.4',
'PREF_LOCALE', 
'ja', 
'Japanese', 
50, 
'2010-08-20 16:17:18.859', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
255, 
'FHNReference',
'3.2.4',
'PREF_LOCALE', 
'nl', 
'Dutch', 
50, 
'2010-08-20 16:17:18.859', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
256, 
'FHNReference',
'3.2.4',
'PREF_LOCALE', 
'pl', 
'Polish', 
50, 
'2010-08-20 16:17:18.859', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
257, 
'FHNReference',
'3.2.4',
'PREF_YN', 
'false', 
'No', 
50, 
'2010-08-20 16:17:18.875', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
258, 
'FHNReference',
'3.2.4',
'PREF_YN', 
'true', 
'Yes', 
50, 
'2010-08-20 16:17:18.875', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
259, 
'FHNReference',
'3.2.4',
'SETTLESYS', 
'EBA', 
'European Bank Association', 
50, 
'2010-06-30 12:14:36.221', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
260, 
'FHNReference',
'3.2.4',
'SP_RANK', 
'PRIMARY', 
'Primary', 
50, 
'2010-06-30 12:14:36.237', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
261, 
'FHNReference',
'3.2.4',
'SP_RANK', 
'SECONDARY', 
'Secondary', 
50, 
'2010-06-30 12:14:36.237', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
262, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'BATCHSOURCE', 
'Batch Payment Source', 
50, 
'2010-06-30 12:14:36.252', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
263, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'BULK_CHECK_PRINT_RECEIVER', 
'Bulk Check Print Receiver', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
264, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'BULK_CPA_RECEIVER', 
'Bulk CPA Receiver', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
265, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'BULK_EDI_RECEIVER', 
'Bulk EDI Receiver', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
266, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'BULK_POSTING_RECEIVER', 
'Bulk Posting Extracts Receiver', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
267, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'BULK_WIRE_RECEIVER', 
'Bulk Wire Receiver', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
268, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'FI_EDI_RECEIVER', 
'FI EDI Receiver', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
269, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'FTM_FEATURE_DIST', 
'Distribution', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
270, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'FTM_FEATURE_INGEST', 
'Ingestion', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
271, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'ISO20022_RECEIVER', 
'ISO20022 Receiver', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
272, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'OP_COMMAND', 
'Ops Admin', 
50, 
'2010-06-30 12:14:36.252', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
273, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'SWIFT_MT_RECEIVER_101', 
'Swift MT Receiver 101', 
50, 
'2010-06-30 12:14:36.252', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
274, 
'FHNReference',
'3.2.4',
'SP_ROLE', 
'SWIFT_MT_RECEIVER_103', 
'Swift MT Receiver 103', 
50, 
'2010-06-30 12:14:36.252', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
275, 
'FHNReference',
'3.2.4',
'TIMEZONE', 
'UTC', 
'UTC Coordinated Universal Time', 
50, 
NULL, 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
276, 
'FHNReference',
'3.2.4',
'TRANSPORT', 
'FILE', 
'File', 
50, 
'2010-06-30 12:14:35.362', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
277, 
'FHNReference',
'3.2.4',
'TRANSPORT', 
'HTTP', 
'HTTP', 
50, 
'2010-06-30 12:14:35.377', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
278, 
'FHNReference',
'3.2.4',
'TRANSPORT', 
'HTTP_SOAP', 
'SOAP over HTTP', 
50, 
'2010-06-30 12:14:35.377', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
279, 
'FHNReference',
'3.2.4',
'TRANSPORT', 
'MAIL', 
'email', 
50, 
'2010-06-30 12:14:35.393', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
280, 
'FHNReference',
'3.2.4',
'TRANSPORT', 
'MQ', 
'MQ', 
50, 
'2010-06-30 12:14:35.409', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
281, 
'FHNReference',
'3.2.4',
'TXNPARTYREL', 
'FORWARDTO', 
'Forwarded to', 
50, 
'2010-06-30 12:14:36.284', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
282, 
'FHNReference',
'3.2.4',
'TXNPARTYREL', 
'INVOLVED', 
'Involved in', 
50, 
'2010-06-30 12:14:36.299', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
283, 
'FHNReference',
'3.2.4',
'TXNPARTYREL', 
'RECVDFROM', 
'Received from', 
50, 
'2010-06-30 12:14:36.299', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
284, 
'FHNReference',
'3.2.4',
'TXNTYPE', 
'COMMAND', 
'Operator Command', 
50, 
'2010-06-30 12:14:36.330', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
285, 
'FHNReference',
'3.2.4',
'TXNTYPE', 
'PAYMENT_INS', 
'Payment Instruction', 
50, 
'2010-06-30 12:14:36.721', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL  UPSERT_CLASSIFICATION (
286, 
'FHNReference',
'3.2.4',
'TXNTYPE', 
'PAYMENT_ORIGINATION', 
'Payment Origination', 
50, 
'2010-06-30 12:14:36.721', 
NULL, 
'N', 
'SCRIPT:FHNReference_config_20210212.sql' 
);



CALL SET_DELETE_FLAG(
'FHNReference',
'3.2.4'
);

COMMIT;